import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ShowDetailDateScreen extends StatefulWidget {
  final String? link;
  const ShowDetailDateScreen({
    super.key,
    required this.link,
  });

  @override
  State<ShowDetailDateScreen> createState() => _ShowDetailDateScreenState();
}

class _ShowDetailDateScreenState extends State<ShowDetailDateScreen> {
  bool isLoading = true;
  late final WebViewController _controller;

  @override
  void initState() {
    super.initState();
    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Colors.white)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
            setState(() {
              isLoading = false;
            });
          },
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      // ..loadRequest(Uri.parse('https://flutter.dev'));
      ..loadRequest(Uri.parse('https://flutter.dev'));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.clearCache();
    _controller.clearLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          WebViewWidget(controller: _controller),
          isLoading
              ? Center(
                  child: Column(
                    children: const [
                      CircularProgressIndicator(),
                      CupertinoActivityIndicator(),
                    ],
                  ),
                )
              : Stack(),
          Positioned(
            top: 44,
            left: 40,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.baby_changing_station,
                color: Colors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
